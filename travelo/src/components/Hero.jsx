import React from 'react'
import styled from 'styled-components';
import homeImage from '../assets/hero.png';

export default function Hero() {
  return (
    <>
      <Her>
         <section id="hero">
            <div className="background">
              <img src={homeImage} alt="hero-background" />
            </div>
            <div className="content">
                <div className="title">
                  <h1>Explore the whole world</h1> 
                  <p> Just check out our daily offers. Feel free to ask any question. We are 24/7 online. Join a journy now! </p>
                </div>
               

                <div className="search">
                  <div className="container">
                       <label htmlFor=""> Where do you want to go ?</label>
                        <input type="text" placeholder="Search your location " />
                  </div>
                  <div className="container">
                       <label htmlFor=""> Check-in</label>
                       <input type="date" />
                  </div>
                  <div className="container">
                       <label htmlFor="">Check-out</label>
                       <input type="date" />
                  </div>
                  <button>Explore Now!</button>
               </div>
                
            </div>
         
         </section> 
      </Her>
    </>
   
  )
}

const Her=styled.section`
  position:relative;
  margin-top:2rem;
  width:100%;
  height:100%;
  .background{
    height:100%;
    img{
      width:100%;
      filter:brightness(0.60)
    }
  }
  .content{
    position:absolute;
    top:0;
    width:100%;
    height:100%;
    z-index:3;
    text-align:center;
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-items:center;
    gap:1rem;
    .title{
      color:#fff;
      h1{
        font-size:3rem;
        leter-spacing:1.2rem
      }
      p{
        text-align:center;
        padding:0 30px;
        margin-top:1.2rem;
        font-size:1.2rem;
      }
    }
    .search{
      display:flex;
      background-color:#ffffffce;
      padding:0.5rem;
      border-radius:0.5rem;
      .container{
        display:flex;
        align-items:center;
        justify-content:center;
        flex-direction:column;
        padding:0 1.5rem;
        label{
          font-size:1.5rem;
          color:#03045e;
        }
        input{
          background-color:transparent;
          border:none;
          color:#000;
          text-align:center;
          &[type="date"]{
            padding-left:3rem
          }
          &::placeholder{
            color:black;
          }
          &:focus{
            outline:none;
          }
        }
      }
      button{
        padding:1rem;
        cursor:pointer;
        border-radius:0.3rem;
        border:none;
        color:#000;
        background-color:#4361ee;
        font-size:1.1rem;
        text-transform:uppercase;
        transition:0.3s ease-in-out;
        &:hover{
          background-color:#023e8a;
        }
      }
    }
  }

  @media screen and (min-width:280px) and (max-width:980px){
    height: 25rem;
    .background{
      img{
        height:100%;
        min-height:400px;
      }
    }
    .content{
      .title{
        h1{
          font-size:1rem;
        }
        p{
          font-size:0.8rem;
          padding: 1vw;
        }
      }
      .search{
        flex-direction:column;
        padding:0.8rem;
        gap:0.8rem;
        .container{
          padding:0 0.8rem;
          input[type="data"]{
            padding-left:1rem;
          }
        }
        button{
          padding:1rem;
          font-size:1rem;
        }
      }
    }
  }
  
`
