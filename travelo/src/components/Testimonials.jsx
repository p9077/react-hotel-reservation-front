import React from 'react';
import styled from 'styled-components';
import avatar from '../assets/avatarImage.jpg';

export default function Testimonials() {
  const runCallback=(cb)=>{
      return cb();
  };
  return (
    <Section id="testimonials">
      <div className="title">
        <h2>Satisfied Customers</h2>
      </div>
      <div className="testimonials">
        { runCallback(()=>{
          const row =[];
           for(var i=0; i<3 ; i++){
              row.push(<div className="testimonial">
              <p> That was such an awsesome Trip {i}! Visit all side of the country by lowest price</p>
              <div className="info">
                <img src={avatar} alt="Saticfied Customer" />
                <details>
                  <h5>Ahmad Khanzadeh</h5>
                  <span> Front End developer</span>
                </details>
              </div>
            </div>
            );
              
            }
            return row;
        })
      
      }
      </div>
    </Section>
  )
}

const Section=styled.section`
margin:0 5px;
.title{
  text-align:center;
  margin-bottom:2rem;
}
.testimonials{
  display:flex;
  justify-content:center;
  margin:0 2rem;
  gap:2rem;
  .testimonial{
    background-color:aliceblue;
    padding:2rem;
    border-radius:0.5rem;
    box-shadow:rgba(100,100,100,0.3) 0px 7px 29px 0px;
    transition:0.3s ease-in-out;
    &:hover{
      transform:translateX(0.4rem) translateY(-1rem);
      box-shadow:rgba(0,0,0,0.35) 0px 5px 15px;
    }
    .info{
      display:flex;
      justify-content:center;
      gap:1rem;
      align-items:center;
      margin-top:1rem;
      img{
        height:3rem;
        border-radius:3rem;
      }
      .details{
        span{
          font-size:0.9rem;
        }
      }
    }
  }
}

@media screen and (min-width:280px) and (max-width:768px){
  .testimonials{
    flex-direction:column;
    margin:0;
    .testimonial{
      justify-content:center;
      .info{
        flex-direction:column;
        justify-content:center;
      }
    }
  }
}
`;
