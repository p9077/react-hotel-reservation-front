import React,{useState} from 'react'
import styled from 'styled-components';
import logo from '../assets/logo.png'

export default function ScrollToTop() {
  // initially dont display scroll to top 
  const[scrollState,setScrollState]= useState(false);
  // a function which scroll the page to top in case of button click
    const toTop= ()=>{
      window.scrollTo({top: 0});
    };
  // if use scroll down more than 200 then display it 
  window.addEventListener("scroll",() =>{
      window.pageYOffset > 200 ? setScrollState(true) : setScrollState(false);
  });

  return (
    <ToTop scrollState={scrollState} onClick={toTop}>
      <img src={logo} alt="" />
    </ToTop>
  )
}


const ToTop=styled.div`
display: ${({ scrollState }) => (scrollState ? "block" : "none")};
position:fixed;
bottom:1rem;
right:2rem;
z-index:10;
cursor:pointer;
img{
  height:1.5rem;
}
border-radius:2rem;
background-color:#1900ff39;
padding:1rem;
`;
